﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Globalization;

namespace prjSistemaComercio
{
    public class Terminal
    {
        public static string ReadString(string variableContext, string objectContext = "", bool noSpaces = false, int minLength = 0, int? maxLength = null)
        {
            Console.Clear();
            if (objectContext != "")
            {
                Console.WriteLine(objectContext);
            }
            Console.Write("(" + variableContext + ") Insira.\n\n=> ");

            string value = "";
            int length;
            bool validationTest;
            Regex regexLetters = new Regex(@"^[a-zA-Z0-9à-úÀ-Ú.@_ ]+$");

            do
            {
                validationTest = true;

                value = Console.ReadLine();
                length = value.Length;

                if (noSpaces)
                {
                    value.Replace(" ", String.Empty);
                }

                if (length < minLength || length > maxLength)
                {
                    validationTest = false;
                }

                if (!regexLetters.IsMatch(value))
                {
                    validationTest = false;
                }

                if (!validationTest)
                {
                    Console.Clear();
                    Console.Write("Inválido!\n\n(" + variableContext + ") Insira novamente.\n\n=> ");
                }
            }
            while (!validationTest);

            return value;
        }

        public static int ReadInt(string variableContext, string objectContext = "", int? min = null, int? max = null, bool clearConsole = true)
        {
            int value = 0;
            bool validationTest = true;

            if (clearConsole)
            {
                Console.Clear();
            }
                
            if (objectContext != "")
            {
                Console.WriteLine(objectContext);
            }
            Console.Write("(" + variableContext + ") Insira.\n\n=> ");

            do
            {
                validationTest = int.TryParse(Console.ReadLine(), out value);

                if (value < min || value > max)
                {
                    validationTest = false;
                }

                if (!validationTest)
                {
                    Console.Clear();
                    if (objectContext != "")
                    {
                        Console.WriteLine(objectContext);
                    }
                    Console.Write("Inválido!\n\n(" + variableContext + ") Insira novamente.\n\n=> ");
                }
            }
            while (!validationTest);

            return value;
        }

        public static float ReadFloat(string variableContext, string objectContext = "", float? min = null, float? max = null)
        {
            float value = 0;
            bool validationTest = true;

            Console.Clear();
            if (objectContext != "")
            {
                Console.WriteLine(objectContext);
            }
            Console.Write("(" + variableContext + ") Insira.\n\n=> ");

            do
            {
                validationTest = float.TryParse(Console.ReadLine(), out value);

                if (value < min || value > max)
                {
                    validationTest = false;
                }

                if (!validationTest)
                {
                    Console.Clear();
                    if (objectContext != "")
                    {
                        Console.WriteLine(objectContext);
                    }
                    Console.Write("Inválido!\n\n(" + variableContext + ") Insira novamente.\n\n=> ");
                }
            }
            while (!validationTest);

            return value;
        }

        static private string ReceiveDate(string variableContext, string objectContext = "")
        {
            List<char> receivedDate = new List<char>();
            Regex regexNumbers = new Regex(@"^[0-9]+$");
            ConsoleKeyInfo entry;
            int length = 0;

            while (length < 10)
            {
                Console.Clear();
                if (objectContext != "")
                {
                    Console.WriteLine(objectContext);
                }
                Console.Write("(" + variableContext + ") Insira.\n\n=> " + String.Join("", receivedDate));
                entry = Console.ReadKey();

                if (regexNumbers.IsMatch(entry.KeyChar.ToString()))
                {
                    receivedDate.Add(entry.KeyChar);

                    length = receivedDate.Count;

                    if (receivedDate.Count == 2)
                    {
                        receivedDate.Add('/');
                    }

                    if (receivedDate.Count == 5)
                    {
                        receivedDate.Add('/');
                    }
                }
                else if ((entry.Key == ConsoleKey.Delete || entry.Key == ConsoleKey.Backspace) && length > 0)
                {
                    length = receivedDate.Count;

                    receivedDate.RemoveAt(length - 1);

                    if (length == 3)
                    {
                        receivedDate.RemoveAt(1);
                    }

                    if (receivedDate.Count == 5)
                    {
                        receivedDate.RemoveAt(4);
                    }
                }
            }

            return String.Join("", receivedDate);
        }

        static public DateTime ReadDate(string variableContext, string objectContext = "", DateTime minDate = new DateTime(), DateTime maxDate = new DateTime())
        {
            DateTime date;

            while
            (
                !DateTime.TryParse(ReceiveDate(variableContext, objectContext), new CultureInfo("pt-BR"), DateTimeStyles.None, out date)
                &&
                Convert.ToInt32(date.Subtract(minDate)) > 0
                &&
                Convert.ToInt32(maxDate.Subtract(date)) > 0
            )
            {
                Console.Write("Inválido!\n\n");
            }

            Console.Clear();

            return date;
        }

        static public bool ReadYesNo(string message)
        {
        	Regex regexYesNo = new Regex(@"^[sn]$");
        	char entry = '0';
        	Console.Write(message + " (s/n)\n\n=> ");

        	while (regexYesNo.IsMatch(entry.ToString())) {
        		entry = Console.ReadKey().KeyChar;
        	}

        	if (entry == 's')
        	{
        		return true;
        	}

        	return false;
        } 
       	
	    static public string ReadPassword(string message = "Informe sua senha")
        {
            string senha = "";
            bool fim = false;

            do
            {
                Console.Clear();
                Console.WriteLine(message + " (Aperte Enter quando finalizar): ");

                ConsoleKeyInfo teclaPressionada = Console.ReadKey();

                if (teclaPressionada.Key.ToString() == "Enter")
                {
                    fim = true;
                }
                else
                {
                    senha += teclaPressionada.KeyChar;
                    Console.WriteLine("Não pressionou enter (senha: " + senha + ")");
                }
            } while (!fim);

            return senha;
        }
    }
}