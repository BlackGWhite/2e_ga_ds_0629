﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prjSistemaComercio
{
    class Produto
    {
    	private bool _ativo;
        private string _nome;
        private string _marca;
        private string _descricao;
        private string _categoria;

        private float _valor;
        private float _valorPromocional;

        public Produto(bool cadastrar = true)
        {
            if (cadastrar)
            {
                this.Cadastrar();
            }
        }

        //
        public void SetAtivo(bool ativo)
        {
        	this._ativo = ativo;
        }

        public bool GetAtivo() {
        	return this._ativo;
        }

        //
        public void SetNome(string nome)
        {
            this._nome = nome;
        }

        public string GetNome()
        {
            return this._nome;
        }

        //
        public void SetMarca(string marca)
        {
            this._marca = marca;
        }

        public string GetMarca()
        {
            return this._marca;
        }

        //
        public void SetDescricao(string descricao)
        {
            this._descricao = descricao;
        }

        public string GetDescricao()
        {
            return this._descricao;
        }

        //
        public void SetCategoria(string categoria)
        {
            this._categoria = categoria;
        }

        public string GetCategoria()
        {
            return this._categoria;
        }

        //
        public void SetValor(float valor)
        {
            this._valor = valor;
        }

        public float GetValor()
        {
            return this._valor;
        }

        //
        public void SetValorPromocional(float valorPromocional)
        {
            this._valorPromocional = valorPromocional;
        }

        public float GetValorPromocional()
        {
            return this._valorPromocional;
        }

        //
        public bool Cadastrar()
        {
            string objectContext = "=== Cadastro de produto ===";

            this.SetAtivo(true);
            this.SetNome(Terminal.ReadString("Nome do produto", objectContext));
            this.SetMarca(Terminal.ReadString("Marca do produto", objectContext));
            this.SetDescricao(Terminal.ReadString("Descrição do produto", objectContext));
            this.SetCategoria(Terminal.ReadString("Categoria do produto", objectContext));
            this.SetValor(Terminal.ReadFloat("Valor do produto", objectContext, 0));

            Console.Clear();
            Console.Write("\nEste produto possui algum valor promocional? (Pressione S para Sim)");
            char resposta = Console.ReadKey().KeyChar;

            if (resposta.ToString().ToLower() == "s")
            {
                this.SetValorPromocional(Terminal.ReadFloat("Valor do produto", objectContext, 0));
            }

            Console.Clear();
            Console.Beep();
            Console.WriteLine("\n\tProduto cadastrado com sucesso!");
            return true;
        }

        public Produto Editar()
        {
            string objectContext = "=== Cadastro de produto ===";
            string success = " atualizado com sucesso! Repetindo...\n";
            int escolha;
            bool sair = false;

            while (!sair)
            {
                Console.WriteLine(
                    objectContext +
                    "\nO que deseja editar?" +
                    "\n1 - Nome" +
                    "\n2 - Marca" +
                    "\n3 - Descrição" +
                    "\n4 - Categoria" +
                    "\n5 - Valor" +
                    "\n6 - Valor promocional" +
                    "\n\n7 - Sair"
                );
                escolha = Terminal.ReadInt("Escolha", "", 1, 7, false);

                switch (escolha)
                {
                    case 1:
                        this.SetNome(Terminal.ReadString("Novo nome do produto", objectContext));
                        Console.WriteLine("Nome" + success);
                        break;

                    case 2:
                        this.SetMarca(Terminal.ReadString("Nova marca do produto", objectContext));
                        Console.WriteLine("Nome de usuário" + success);
                        break;

                    case 3:
                        this.SetDescricao(Terminal.ReadString("Nova descrição do produto", objectContext, true, 8));
                        Console.WriteLine("Senha" + success);
                        break;

                    case 4:
                        this.SetCategoria(Terminal.ReadString("Nova categoria do produto", objectContext));
                        Console.WriteLine("Data de nascimento" + success);
                        break;

                    case 5:
                        this.SetValor(Terminal.ReadFloat("Novo valor do produto", objectContext));
                        Console.WriteLine("Endereço" + success);
                        break;

                    case 6:
                        this.SetValorPromocional(Terminal.ReadFloat("Novo valor promocional do produto", objectContext));
                        Console.WriteLine("CPF" + success);
                        break;

                    case 7:
                        sair = true;
                        break;

                    default:
                        Console.WriteLine("Opção inválida! Repetindo...\n");
                        break;
                }
            }

            Console.Clear();
            Console.Beep();
            Console.WriteLine("\n\tProduto cadastrado com sucesso!");

            return this;
        }

        public void Excluir()
        {
            this.SetAtivo(false);
            this.SetNome("");
            this.SetMarca("");
            this.SetDescricao("");
            this.SetCategoria("");
            this.SetValor(0);
            this.SetValorPromocional(0);
        }

        public void MostrarDetalhes()
        {
            Console.WriteLine(
            	"\n=== Detalhes do Produto ===" +
	            "\nNome: " + this._nome +
	            "\nMarca: " + this._marca +
	            "\nDescrição: " + this._descricao +
	            "\nCategoria: " + this._categoria +
	            "\nValores:" +
	            "\n\tPreço: R$ " + this._valor.ToString("F2")
        	);

            if (this._valorPromocional > 0)
            {
                Console.WriteLine("\tValor Promocional: R$ " + this._valorPromocional.ToString("F2"));
            }
        }
    }
}