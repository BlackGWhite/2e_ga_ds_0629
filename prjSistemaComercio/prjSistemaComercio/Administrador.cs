﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prjSistemaComercio
{
    class Administrador
    {
        private bool _ativo;
        private string _nome;
        private string _nomeUsuario;
        private string _senha;
        private DateTime _dataNascimento;
        private string _endereco;
        private string _cpf;
        private float _salario;

        public Administrador(bool cadastrar = true)
        {
            if (cadastrar)
            {
                this.Cadastrar();
            }
        }

        //
        public void SetAtivo(bool ativo)
        {
            this._ativo = ativo;
        }

        public bool GetAtivo()
        {
            return this._ativo;
        }

        //
        public void SetNome(string nome)
        {
            this._nome = nome;
        }

        public string GetNome()
        {
            return this._nome;
        }

        //
        public void SetNomeUsuario(string nomeUsuario)
        {
            this._nomeUsuario = nomeUsuario;
        }

        public string GetNomeUsuario()
        {
            return this._nomeUsuario;
        }

        //
        public void SetSenha(string senha)
        {
            this._senha = senha;
        }

        public string GetSenha()
        {
            return this._senha;
        }

        //
        public void SetDataNascimento(DateTime dataNascimento)
        {
            this._dataNascimento = dataNascimento;
        }

        public DateTime GetDataNascimento()
        {
            return this._dataNascimento;
        }

        //
        public void SetEndereco(string endereco)
        {
            this._endereco = endereco;
        }

        public string GetEndereco()
        {
            return this._endereco;
        }

        //
        public void SetCPF(string cpf)
        {
            this._cpf = cpf;
        }

        public string GetCPF()
        {
            return this._cpf;
        }

        //
        public void SetSalario(float salario)
        {
            this._salario = salario;
        }

        public float GetSalario()
        {
            return this._salario;
        }

        public void Cadastrar()
        {
            string objectContext = "=== Cadastro de administrador ===";

            this.SetAtivo(true);
            this.SetNome(Terminal.ReadString("Nome do administrador", objectContext));
            this.SetNomeUsuario(Terminal.ReadString("Nome de usuário do administrador", objectContext));
            this.SetSenha(Terminal.ReadString("Senha do administrador", objectContext, true, 8));
            this.SetDataNascimento(Terminal.ReadDate("Data de nascimento do administrador", objectContext));
            this.SetEndereco(Terminal.ReadString("Endereço do administrador", objectContext));
            this.SetCPF(Terminal.ReadString("CPF do administrador", objectContext));
            this.SetSalario(Terminal.ReadFloat("Salário do administrador", objectContext, 0));

            Console.Clear();
            Console.Beep();
            Console.WriteLine("\n\tAdministrador cadastrado com sucesso!");
        }

        public Administrador Editar()
        {
            string objectContext = "=== Cadastro de administrador ===";
            string success = " atualizado com sucesso! Repetindo...\n";
            int escolha;
            bool sair = false;

            while (!sair)
            {
                Console.WriteLine(
                    objectContext +
                    "\nO que deseja editar?" +
                    "\n1 - Nome" +
                    "\n2 - Nome de usuário" +
                    "\n3 - Senha" +
                    "\n4 - Data de nascimento" +
                    "\n5 - Endereço" +
                    "\n6 - CPF" +
                    "\n7 - Salário" +
                    "\n\n8 - Sair"
                );
                escolha = Terminal.ReadInt("Escolha", "", 1, 8, false);

                switch (escolha)
                {
                    case 1:
                        this.SetNome(Terminal.ReadString("Novo nome do administrador", objectContext));
                        Console.WriteLine("Nome" + success);
                        break;

                    case 2:
                        this.SetNomeUsuario(Terminal.ReadString("Novo nome de usuário do administrador", objectContext));
                        Console.WriteLine("Nome de usuário" + success);
                        break;

                    case 3:
                        this.SetSenha(Terminal.ReadString("Nova senha do administrador", objectContext, true, 8));
                        Console.WriteLine("Senha" + success);
                        break;

                    case 4:
                        this.SetDataNascimento(Terminal.ReadDate("Nova data de nascimento do administrador", objectContext));
                        Console.WriteLine("Data de nascimento" + success);
                        break;

                    case 5:
                        this.SetEndereco(Terminal.ReadString("Novo endereço do administrador", objectContext));
                        Console.WriteLine("Endereço" + success);
                        break;

                    case 6:
                        this.SetCPF(Terminal.ReadString("Novo CPF do administrador", objectContext));
                        Console.WriteLine("CPF" + success);
                        break;

                    case 7:
                        this.SetSalario(Terminal.ReadFloat("Novo salário do administrador", objectContext, 0));
                        Console.WriteLine("Salário" + success);
                        break;

                    case 8:
                        sair = true;
                        break;

                    default:
                        Console.WriteLine("Opção inválida! Repetindo...\n");
                        break;
                }
            }

            Console.Clear();
            Console.Beep();
            Console.WriteLine("\n\tAdministrador cadastrado com sucesso!");

            return this;
        }

        public void Excluir()
        {
            this.SetAtivo(false);
            this.SetNome("");
            this.SetNomeUsuario("");
            this.SetSenha("");
            this.SetDataNascimento(new DateTime());
            this.SetEndereco("");
            this.SetCPF("");
            this.SetSalario(0);
        }

        public void MostrarDados()
        {
            Console.WriteLine(
                "\n=== Dados do administrador ===" +
                "\nNome: " + this._nome +
                "\nNome de usuário: " + this._nomeUsuario +
                "\nData de nascimento: " + this._dataNascimento +
                "\nEndereço: " + this._endereco +
                "\nCPF: " + this._cpf +
                "\nSalário: R$ " + this._salario.ToString("F2")
            );
        }

        public static void MostrarMenu(Database database, Usuario usuario)
        {
            bool fimPrograma = false;

            do
            {
                Console.Clear();
                Console.WriteLine("\t=== Menu do Administrador ===");
                Console.WriteLine("\n1- Adicionar administrador\n2- Editar administrador\n3- Excluir administrador\n------------------------------------------------\n4- Adicionar funcionário\n5- Editar funcionário\n6- Excluir funcionário\n------------------------------------------------\n7- Adicionar cliente\n8- Editar cliente\n9- Excluir cliente\n------------------------------------------------\n10- Adicionar produto\n11- Editar produto\n12- Excluir produto\n13- Visualizar produto\n14- Listar produtos\n\nDigite 'Exit' para Sair");

                string teclaPressionada = Console.ReadLine();

                if (teclaPressionada.ToLower() == "exit")
                {
                    fimPrograma = true;
                    return;
                }

                int opcaoEscolhida;
                if (int.TryParse(teclaPressionada, out opcaoEscolhida))
                {
                    int usuarioId;
                    int produtoId;

                    switch (opcaoEscolhida)
                    {
                        case 1:
                            database.AdicionarAdministrador();
                            break;

                        case 2:
                            database.ListarAdministradores();

                            Console.WriteLine("Pressione qualquer tecla para continuar");
                            Console.ReadKey();

                            usuarioId = Terminal.ReadInt("Qual o ID da conta que você deseja alterar?", "", 1, database._administradores.Count());
                            database.EditarAdministrador(usuarioId - 1);

                            break;

                        case 3:
                            database.ListarAdministradores();

                            Console.WriteLine("Pressione qualquer tecla para continuar");
                            Console.ReadKey();

                            usuarioId = Terminal.ReadInt("Qual o ID da conta que você deseja excluir?", "", 1, database._administradores.Count());
                            database.ExcluirAdministrador(usuarioId - 1);

                            break;

                        case 4:
                            database.AdicionarFuncionario();

                            break;

                        case 5:
                            database.ListarFuncionarios();

                            Console.WriteLine("Pressione qualquer tecla para continuar");
                            Console.ReadKey();

                            usuarioId = Terminal.ReadInt("Qual o ID da conta que você deseja editar?", "", 1, database._funcionarios.Count());
                            database.EditarFuncionario(usuarioId - 1);

                            break;

                        case 6:
                            database.ListarFuncionarios();

                            Console.WriteLine("Pressione qualquer tecla para continuar");
                            Console.ReadKey();

                            usuarioId = Terminal.ReadInt("Qual o ID da conta que você deseja excluir?", "", 1, database._funcionarios.Count());
                            database.ExcluirFuncionario(usuarioId - 1);

                            break;

                        case 7:
                            database.AdicionarCliente();
                            break;

                        case 8:
                            database.ListarClientes();

                            Console.WriteLine("Pressione qualquer tecla para continuar");
                            Console.ReadKey();

                            usuarioId = Terminal.ReadInt("Qual o ID da conta que você deseja editar?", "", 1, database._clientes.Count());
                            database.EditarCliente(usuarioId - 1);

                            break;

                        case 9:
                            database.ListarClientes();

                            Console.WriteLine("Pressione qualquer tecla para continuar");
                            Console.ReadKey();
                            
                            usuarioId = Terminal.ReadInt("Qual o ID da conta que você deseja excluir?", "", 1, database._clientes.Count());
                            database.ExcluirCliente(usuarioId - 1);

                            break;

                        case 10:
                            database.AdicionarProduto();
                            break;

                        case 11:
                            database.ListarProdutos();
                            
                            Console.WriteLine("Pressione qualquer tecla para continuar");
                            Console.ReadKey();

                            produtoId = Terminal.ReadInt("Qual o ID do produto que você deseja editar?", "", 1, database._clientes.Count());
                            database.EditarProduto(produtoId - 1);

                            break;

                        case 12:
                            database.ListarProdutos();
                            
                            Console.WriteLine("Pressione qualquer tecla para continuar");
                            Console.ReadKey();

                            produtoId = Terminal.ReadInt("Qual o ID do produto que você deseja excluir?", "", 1, database._clientes.Count());
                            database.ExcluirProduto(produtoId - 1);

                            break;

                        case 13:
                            database.ListarProdutos();
                            
                            Console.WriteLine("Pressione qualquer tecla para continuar");
                            Console.ReadKey();

                            produtoId = Terminal.ReadInt("Qual o ID do produto que você deseja visualizar?", "", 1, database._clientes.Count());
                            database._produtos[produtoId - 1].MostrarDetalhes();

                            break;

                        case 14:
                            database.ListarProdutos();

                            Console.ReadKey();
                            break;

                        default:
                            Console.WriteLine("Ops! Opção inválida. Tente novamente");
                            break;
                    }
                }
            } while (fimPrograma == false);
        }
    }
}

