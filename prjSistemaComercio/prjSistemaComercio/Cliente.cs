﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prjSistemaComercio
{
    class Cliente
    {
        private bool _ativo;
        private string _nome;
        private string _nomeUsuario;
        private string _senha;
        private DateTime _dataNascimento;
        private string _endereco;
        private string _cpf;

        public Cliente(bool cadastrar = true)
        {
            if (cadastrar)
            {
                this.Cadastrar();
            }
        }

        //
        public void SetAtivo(bool ativo)
        {
            this._ativo = ativo;
        }

        public bool GetAtivo()
        {
            return this._ativo;
        }

        //
        public void SetNome(string nome)
        {
            this._nome = nome;
        }

        public string GetNome()
        {
            return this._nome;
        }

        //
        public void SetNomeUsuario(string nomeUsuario)
        {
            this._nomeUsuario = nomeUsuario;
        }

        public string GetNomeUsuario()
        {
            return this._nomeUsuario;
        }

        //
        public void SetSenha(string senha)
        {
            this._senha = senha;
        }

        public string GetSenha()
        {
            return this._senha;
        }

        //
        public void SetDataNascimento(DateTime dataNascimento)
        {
            this._dataNascimento = dataNascimento;
        }

        public DateTime GetDataNascimento()
        {
            return this._dataNascimento;
        }

        //
        public void SetEndereco(string endereco)
        {
            this._endereco = endereco;
        }

        public string GetEndereco()
        {
            return this._endereco;
        }

        //
        public void SetCPF(string cpf)
        {
            this._cpf = cpf;
        }

        public string GetCPF()
        {
            return this._cpf;
        }

        public void Cadastrar()
        {
            string objectContext = "=== Cadastro de cliente ===";

            this.SetAtivo(true);
            this.SetNome(Terminal.ReadString("Nome do cliente", objectContext));
            this.SetNomeUsuario(Terminal.ReadString("Nome de usuário do cliente", objectContext));
            this.SetSenha(Terminal.ReadString("Senha do cliente", objectContext, true, 8));
            this.SetDataNascimento(Terminal.ReadDate("Data de nascimento do cliente", objectContext));
            this.SetEndereco(Terminal.ReadString("Endereço do cliente", objectContext));
            this.SetCPF(Terminal.ReadString("CPF do cliente", objectContext));

            Console.Clear();
            Console.Beep();
            Console.WriteLine("\nCliente cadastrado com sucesso!");
        }

        public Cliente Editar()
        {
            string objectContext = "=== Cadastro de cliente ===";
            string success = " atualizado com sucesso! Repetindo...\n";
            int escolha;
            bool sair = false;

            while (!sair)
            {
                Console.WriteLine(
                    objectContext +
                    "\nO que deseja editar?" +
                    "\n1 - Nome" +
                    "\n2 - Nome de usuário" +
                    "\n3 - Senha" +
                    "\n4 - Data de nascimento" +
                    "\n5 - Endereço" +
                    "\n6 - CPF" +
                    "\n\n7 - Sair"
                );
                escolha = Terminal.ReadInt("Escolha", "", 1, 7, false);

                switch (escolha)
                {
                    case 1:
                        this.SetNome(Terminal.ReadString("Novo nome do cliente", objectContext));
                        Console.WriteLine("Nome" + success);
                        break;

                    case 2:
                        this.SetNomeUsuario(Terminal.ReadString("Novo nome de usuário do cliente", objectContext));
                        Console.WriteLine("Nome de usuário" + success);
                        break;

                    case 3:
                        this.SetSenha(Terminal.ReadString("Nova senha do cliente", objectContext, true, 8));
                        Console.WriteLine("Senha" + success);
                        break;

                    case 4:
                        this.SetDataNascimento(Terminal.ReadDate("Nova data de nascimento do cliente", objectContext));
                        Console.WriteLine("Data de nascimento" + success);
                        break;

                    case 5:
                        this.SetEndereco(Terminal.ReadString("Novo endereço do cliente", objectContext));
                        Console.WriteLine("Endereço" + success);
                        break;

                    case 6:
                        this.SetCPF(Terminal.ReadString("Novo CPF do cliente", objectContext));
                        Console.WriteLine("CPF" + success);
                        break;

                    case 7:
                        sair = true;
                        break;

                    default:
                        Console.WriteLine("Opção inválida! Repetindo...\n");
                        break;
                }
            }

            Console.Clear();
            Console.Beep();
            Console.WriteLine("\n\tcliente cadastrado com sucesso!");

            return this;
        }

        public void Excluir()
        {
            this.SetAtivo(false);
            this.SetNome("");
            this.SetNomeUsuario("");
            this.SetSenha("");
            this.SetDataNascimento(new DateTime());
            this.SetEndereco("");
            this.SetCPF("");
        }

        public void MostrarDados()
        {
            Console.WriteLine(
                "=== Dados do cliente ===" +
                "\nNome: " + this._nome +
                "\nNome de usuário: " + this._nomeUsuario +
                "\nData de nascimento: " + this._dataNascimento +
                "\nEndereço: " + this._endereco +
                "\nCPF: " + this._cpf
            );
        }
    }
}