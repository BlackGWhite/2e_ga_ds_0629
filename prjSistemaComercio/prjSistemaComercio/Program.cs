﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prjSistemaComercio
{
    class Program
    {
        static void Main(string[] args)
        {
            Database database = new Database();
            Usuario usuario = new Usuario(database);

            while (!usuario.isLogged())
            {
                usuario.Login(database);
            }

            if (usuario.verificarPermissao(0))
            {
                Administrador.MostrarMenu(database, usuario);
            }
            else if (usuario.verificarPermissao(1))
            {
                Funcionario.MostrarMenu(database, usuario);
            }
            else
            {
                Console.WriteLine("Você não tem permissão");
            }

            Console.Clear();
            Console.WriteLine("\t === FIM ===");
            Console.ReadKey();
        }
    }
}
