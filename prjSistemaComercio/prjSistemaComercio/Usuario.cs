﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prjSistemaComercio
{
    class Usuario
    {
        static private string[] _CARGOS = { "Administrador", "Funcionário", "Cliente" };

        private bool _logado;

        private string _nome;
        private string _senha;

        private int _cargoId;

        public Usuario(Database database)
        {
            this.Login(database);
        }

        //
        public bool isLogged()
        {
            return this._logado;
        }

        //
        public void setNome(string nome)
        {
            this._nome = nome;
        }

        public string getNome()
        {
            return this._nome;
        }

        //
        public void setSenha(string senha)
        {
            this._nome = senha;
        }

        //
        private void setCargoId(int cargoId)
        {
            if (cargoId < 0 || cargoId >= Usuario._CARGOS.Length)
            {
                Console.WriteLine("Ops! Houve um problema com o cargo do usuário");
                this._cargoId = 0;
            }
            else
            {
                this._cargoId = cargoId;
            }
        }

        public int getCargoId()
        {
            return this._cargoId;
        }

        //
        public bool verificarSenha(string senhaParaVerificar)
        {
            if (this._senha == senhaParaVerificar)
            {
                return true;
            }

            return false;
        }

        //
        public bool verificarPermissao(int permissaoNecessaria)
        {
            if (!this.isLogged())
            {
                return false;
            }

            if (permissaoNecessaria >= this._cargoId)
            {
                return true;
            }

            return false;
        }

        //
        public bool Login(Database database)
        {
            this._logado = false;
            bool sucesso = false;

            int tipoLogin = Terminal.ReadInt("Qual será seu tipo de login?\n1- Administrador\n2- Funcionário\n3- Cliente", "=== Login ===", 1, 3);
            string nomeUsuario = Terminal.ReadString("Informe seu nome de usuário");
            string senha = Terminal.ReadPassword("Informe sua senha");

            int idAccount;

            switch (tipoLogin)
            {
                case 1:
                    idAccount = database._administradores.FindIndex(p => p.GetNomeUsuario() == nomeUsuario);

                    if (idAccount != -1 && database._administradores[idAccount].GetSenha() == senha)
                    {
                        //Sucesso no login
                        sucesso = true;
                        Administrador conta = database._administradores[idAccount];

                        this._nome = conta.GetNomeUsuario();
                        this._senha = conta.GetSenha();

                        Console.WriteLine("Login efetuado com sucesso");
                    }
                    else
                    {
                        Console.Write("Nome de usuário ou senha incorretos!");
                        Console.ReadKey();
                    }

                    break;

                case 2:
                    idAccount = database._funcionarios.FindIndex(p => p.GetNomeUsuario() == nomeUsuario);

                    if (idAccount != -1 && database._funcionarios[idAccount].GetSenha() == senha)
                    {
                        //Sucesso no login
                        sucesso = true;
                        Funcionario conta = database._funcionarios[idAccount];

                        this._nome = conta.GetNomeUsuario();
                        this._senha = conta.GetSenha();
                    }
                    else
                    {
                        Console.Write("Nome de usuário ou senha incorretos!");
                        Console.ReadKey();
                    }

                    break;

                case 3:
                    idAccount = database._clientes.FindIndex(p => p.GetNomeUsuario() == nomeUsuario);
                    
                    if (idAccount != -1 && database._clientes[idAccount].GetSenha() == senha)
                    {
                        //Sucesso no login
                        sucesso = true;
                        Funcionario conta = database._funcionarios[idAccount];

                        this._nome = conta.GetNomeUsuario();
                        this._senha = conta.GetSenha();
                    }
                    else
                    {
                        Console.Write("Nome de usuário ou senha incorretos!");
                        Console.ReadKey();
                    }

                    break;

                default:
                    Console.WriteLine("Ops! Houve um erro");
                    break;
            }

            if (sucesso)
            {
                this._logado = true;
                this._cargoId = tipoLogin - 1;
            }

            return sucesso;
        }
    }
}