﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prjSistemaComercio
{
    class Database
    {
    	public List<Administrador> _administradores = new List<Administrador>();
        public List<Funcionario> _funcionarios = new List<Funcionario>();
        public List<Cliente> _clientes = new List<Cliente>();
        public List<Produto> _produtos = new List<Produto>();

        public Database()
        {
        	Administrador administrador1 = new Administrador(false);
        	administrador1.SetAtivo(true);
            administrador1.SetNome("Admin");
            administrador1.SetNomeUsuario("Admin");
            administrador1.SetSenha("Admin");
            administrador1.SetDataNascimento(new DateTime());
            administrador1.SetEndereco("Admin");
            administrador1.SetCPF("Admin");
            administrador1.SetSalario(0.00F);

            Administrador administrador2 = new Administrador(false);
            administrador2.SetAtivo(true);
            administrador2.SetNome("Jaqueline Sophia Simone Almada");
            administrador2.SetNomeUsuario("jaquelineAlmada96");
            administrador2.SetSenha("dn09Nfz9fQ");
            administrador2.SetDataNascimento(DateTime.Parse("12/03/1965"));
            administrador2.SetEndereco("Rua Manoel Correa de Almeida, 615, Cristo Rei, Várzea Grande - MT");
            administrador2.SetCPF("87043908302");
            administrador2.SetSalario(9895.00F);

            Funcionario funcionario1 = new Funcionario(false);
            funcionario1.SetAtivo(true);
            funcionario1.SetNome("Samuel Augusto Murilo Moreira");
            funcionario1.SetNomeUsuario("samuelAugusto");
            funcionario1.SetSenha("HLX02JDpeG");
            funcionario1.SetCargo("Desempregado");
            funcionario1.SetDataNascimento(DateTime.Parse("10/04/1977"));
            funcionario1.SetEndereco("Rua Antônio Dalva, 154, Nova Esperança, Manaus - AM");
            funcionario1.SetCPF("27308887723");
            funcionario1.SetSalario(2100.00F);

            Funcionario funcionario2 = new Funcionario(false);
            funcionario2.SetAtivo(true);
            funcionario2.SetNome("Eloá Betina Assunção");
            funcionario2.SetNomeUsuario("eloaBetina");
            funcionario2.SetSenha("BM7mLF6bkq");
            funcionario2.SetCargo("Desempregada");
            funcionario2.SetDataNascimento(DateTime.Parse("01/05/1958"));
            funcionario2.SetEndereco("Quadra Dezesseis, 601, Tabajara Brites, Uruguaiana - RS");
            funcionario2.SetCPF("32571462482");
            funcionario2.SetSalario(2300.00F);

            Cliente cliente1 = new Cliente(false);
            cliente1.SetAtivo(true);
            cliente1.SetNome("Heitor Kaique Bryan Porto");
            cliente1.SetNomeUsuario("heitorKaique");
            cliente1.SetSenha("8NCuELl5g8");
            cliente1.SetDataNascimento(DateTime.Parse("03/26/1972"));
            cliente1.SetEndereco("Rua Trinta e Dois, 287, Loteamento Jardim Florença, Gravataí - RS");
            cliente1.SetCPF("88333473113");

            Cliente cliente2 = new Cliente(false);
            cliente2.SetAtivo(true);
            cliente2.SetNome("Nair Cláudia Sabrina Assunção");
            cliente2.SetNomeUsuario("nairAssuncao");
            cliente2.SetSenha("Pqm4kZ7cdd");
            cliente2.SetDataNascimento(DateTime.Parse("05/05/1993"));
            cliente2.SetEndereco("Avenida General Djenal Tavares de Queroz, 974, Luzia, Aracaju - SE");
            cliente2.SetCPF("08051738462");

			Produto hotWilson = new Produto(false);
            hotWilson.SetMarca("Hot Wilson");
            hotWilson.SetNome("Pista Hot Wilson + Brasília Amarela");
            hotWilson.SetCategoria("Brinquedos");
            hotWilson.SetDescricao("Uma enorme serpente destila Slime verde de suas presas no conjunto Ataque Tóxico da Serpente Hot Wilson. As crianças lançam seus carros da Hot Wilson através da cobra enquanto tentam alcançar um dos dois botões que a destroem");
            hotWilson.SetAtivo(true);
            hotWilson.SetValor(224.99F);

            Produto bancoImobiliario = new Produto(false);
            bancoImobiliario.SetMarca("Estrela");
            bancoImobiliario.SetNome("Banco Imobiliário");
            bancoImobiliario.SetCategoria("Brinquedos");
            bancoImobiliario.SetDescricao("O mercado de imóveis se modernizou e está cada vez mais competitivo. Diversificar os investimentos virou palavra de ordem. O banco imobiliário traz todo o dinamismo do mundo dos negócios para os dias de hoje.");
            bancoImobiliario.SetAtivo(true);
            bancoImobiliario.SetValor(24.99F);

            this._administradores.Add(administrador1);
            this._administradores.Add(administrador2);
            this._funcionarios.Add(funcionario1);
            this._funcionarios.Add(funcionario2);
            this._clientes.Add(cliente1);
            this._clientes.Add(cliente2);
            this._produtos.Add(hotWilson);
            this._produtos.Add(bancoImobiliario);
        }

        // Administrador

        public void AdicionarAdministrador()
        {
            Administrador administrador = new Administrador();

            int emptyID = this._administradores.FindIndex(p => p.GetAtivo() == false);

            if (emptyID == -1)
            {
                this._administradores.Add(administrador);
            }
            else
            {
                this._administradores[emptyID] = administrador;
            }
        }

        public void ListarAdministradores()
        {
            int counter = 0;

            Console.WriteLine("\t=== Administradores ===\n");

            foreach(Administrador administrador in this._administradores)
            {
                counter++;

                Console.WriteLine("\tAdministrador (ID: "+ counter +")");
                Console.WriteLine("Nome: " + administrador.GetNome() + "\n\n");
            }
        }

        public void EditarAdministrador(int id)
        {
            if (this._administradores.Count() > id && id >= 0)
            {
                Administrador administradorAtualizado = this._administradores[id].Editar();
                this._administradores[id] = administradorAtualizado;
            }
            else
            {
                Console.WriteLine("Não existe nenhuma conta com este ID");
                Console.ReadKey();
            }
        }

        public void ExcluirAdministrador(int id)
        {
            Console.Write("Administrador excluído com sucesso!");
            this._administradores.RemoveAt(id);
            Console.ReadKey();
        }

        // Funcionário

        public void AdicionarFuncionario()
        {
            Funcionario funcionario = new Funcionario();

            int emptyID = this._funcionarios.FindIndex(p => p.GetAtivo() == false);

            if (emptyID == -1)
            {
                this._funcionarios.Add(funcionario);
            }
            else
            {
                this._funcionarios[emptyID] = funcionario;
            }

            Console.WriteLine("\t== Funcionário cadastrado com sucesso ==");
            Console.ReadKey();
        }

        public void ListarFuncionarios()
        {
            int counter = 0;

            Console.WriteLine("\t=== Funcionários ===\n");

            foreach (Funcionario funcionario in this._funcionarios)
            {
                counter++;

                Console.WriteLine("\tFuncionário (ID: " + counter + ")");
                Console.WriteLine("Nome: " + funcionario.GetNome() + "\n\n");

            }
        }

        public void EditarFuncionario(int id)
        {
            Funcionario funcionarioAtualizado = this._funcionarios[id].Editar();
            this._funcionarios[id] = funcionarioAtualizado;
        }

        public void ExcluirFuncionario(int id)
        {
            Console.Write("Funcionario excluído com sucesso!");
            this._funcionarios.RemoveAt(id);
            Console.ReadKey();
        }

        // Cliente

        public void AdicionarCliente()
        {
            Cliente cliente = new Cliente();

            int emptyID = this._clientes.FindIndex(p => p.GetAtivo() == false);

            if (emptyID == -1)
            {
                this._clientes.Add(cliente);
            }
            else
            {
                this._clientes[emptyID] = cliente;
            }
        }

        public void ListarClientes()
        {
            int counter = 0;

            Console.WriteLine("\t=== Clientes ===\n");

            foreach (Cliente cliente in this._clientes)
            {
                counter++;

                Console.WriteLine("\n\tCliente (ID: " + counter + ")");
                cliente.MostrarDados();
            }
        }

        public void EditarCliente(int id)
        {
            Cliente clienteAtualizado = this._clientes[id].Editar();
            this._clientes[id] = clienteAtualizado;
        }

        public void ExcluirCliente(int id)
        {
            Console.Write("Cliente excluído com sucesso!");
            this._clientes.RemoveAt(id);

            Console.ReadKey();
        }

        // Produto

        public void AdicionarProduto()
        {
            Produto produto = new Produto();

            int emptyID = this._produtos.FindIndex(p => p.GetAtivo() == false);

            if (emptyID == -1)
            {
                this._produtos.Add(produto);
            }
            else
            {
                this._produtos[emptyID] = produto;
            }
        }

        public void EditarProduto(int id)
        {
            Produto produtoAtualizado = this._produtos[id].Editar();
            this._produtos[id] = produtoAtualizado;
        }

        public void ExcluirProduto(int id)
        {
            Console.Write("Produto excluído com sucesso!");
            this._produtos.RemoveAt(id);

            Console.ReadKey();
        }

        public void VisualizarProduto(int id)
        {
            this._produtos[id].MostrarDetalhes();
        }

        public void ListarProdutos()
        {
            int counter = 0;

            Console.WriteLine("\t=== Produtos ===\n");


            foreach (Produto produto in this._produtos)
            {
                counter++;

                Console.WriteLine("\n\tProduto (ID: " + counter + ")");
                Console.WriteLine("Nome: " + produto.GetNome() + "\n");
            }
        }
    }
}
